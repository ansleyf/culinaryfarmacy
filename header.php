<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">

<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="distribution" content="global" />
<meta name="robots" content="follow, all" />
<meta name="language" content="en" />
<meta name="verify-v1" content="7XvBEj6Tw9dyXjHST/9sgRGxGymxFdHIZsM6Ob/xo5E=" />

<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
<link rel="Shortcut Icon" href="<?php echo bloginfo('template_url'); ?>/images/favicon.ico" type="image/x-icon" />

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<?php get_template_part( 'style', 'options' ); ?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/custom.css?v=2" type="text/css" media="screen" />

<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> <?php _e("RSS Feed", 'organicthemes'); ?>" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> <?php _e("Atom Feed", 'organicthemes'); ?>" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_enqueue_script("jquery"); ?>
<?php wp_head(); ?>

 <script type="text/javascript" src="//use.typekit.net/lwj7ciz.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/superfish/superfish.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/superfish/hoverIntent.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.anythingslider.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.anythingslider.video.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/swfobject.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/common.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/popup.js"></script>
<link href='http://fonts.googleapis.com/css?family=Playfair+Display' rel='stylesheet' type='text/css'>

<script type="text/javascript">
	var $j = jQuery.noConflict();
	function formatText(index, panel) {             
		var title = $j(panel).find("h4").text();         
	    return title;
	};
	$j(document).ready(function () {
		$j('#slider1').anythingSlider({
			width           : 950,
			height          : 400,
			delay           : <?php echo of_get_option('slider_transition_interval'); ?>,
			resumeDelay     : 10000,
			startStopped    : false,
			autoPlay        : true,
			autoPlayLocked  : false,
			easing          : "swing",
			navigationFormatter : formatText					
		});
	});
</script>

<script type="text/javascript"> 
	var $j = jQuery.noConflict();
	$j(document).ready(function () {
		$j('#homeslider iframe').each(function() {
			var url = $j(this).attr("src")
			$j(this).attr("src",url+"&amp;wmode=Opaque")
		});
	});
</script>

<script type="text/javascript"> 
	var $j = jQuery.noConflict();
     $j(document).ready(function() { 
        $j('.menu').superfish(); 
    }); 
</script>

</head>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<body <?php if(function_exists('body_class')) body_class(); ?>>

<header>
	<div id="navbar">
		<div class="wrap">
		<div id="social-icons">
			<a id="facebook" class="social-icon icon-facebook" href="https://www.facebook.com/CulinaryFarmacy" target="_blank">&nbsp;</a>
			<a id="twitter" class="social-icon icon-twitter" href="https://twitter.com/CulinaryFarmacy" target="_blank">&nbsp;</a>
		</div>

 		<?php if ( function_exists('ot_register_menu') ) { // Check for 3.0+ menus
	        	wp_nav_menu( array( 
	        		'theme_location' => 'header-menu', 
	        		'title_li' => '', 
	        		'depth' => 4, 
	        		'container_class' => 'menu' 
	        		)
	        	); 
	        } else { ?>
	        	<ul class="menu"><?php wp_list_pages('title_li=&depth=4'); ?></ul>
	        <?php } ?>
        </div>
    </div>
    <div id="header">
    	<div class="wrap">
        	<h1 id="title"><a href="<?php echo get_option('home'); ?>/" title="Home"> </a></h1>
        	<div class="hero"><img src="<?php bloginfo('template_url'); ?>/images/arrow.png"><h2>Start your journey now!</h2></div>
    	</div>
    </div>

	<div id="optin">
		<div class="wrap">
    		<?php get_template_part( 'mailchimp' ); ?>
		</div>
	</div>
    <div class="clear"></div>
</header>
<div id="wrap">