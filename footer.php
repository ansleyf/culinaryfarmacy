<div id="footer">

	<div id="footer_border">

    <div id="footerwidgets">
        
            <div class="widgetleft">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Left') ) : ?> 
                <?php endif; ?>
            </div>
            
            <div class="widgetmid">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Mid') ) : ?>
                <?php endif; ?>
            </div>
            
            <div class="widgetright">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Right') ) : ?>
                <?php endif; ?>
            </div>
            
    </div>

	<div id="footerinfo">
    
    	<div class="footerleft">
            
            <p><?php _e("Copyright", 'organicthemes'); ?> &copy; <?php echo date(__("Y", 'organicthemes')); ?> &middot; <?php _e("All Rights Reserved", 'organicthemes'); ?> &middot; <?php bloginfo('name'); ?></p>

           
            
        </div>
        
        <div class="footerright">
        
    		<a href="http://www.organicthemes.com" title="Designer WordPress Themes" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/footer_logo.png" alt="<?php _e("Organic Themes",'organicthemes'); ?>" /></a>
    		
    	</div>
		
	</div>
    
    </div>

</div>

<div class="clear"></div>

</div>

<?php do_action('wp_footer'); ?>

</body>
</html>