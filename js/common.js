var selected_id = null;
var dom = document.getElementById; 
function swaptab(id) { 
	if (dom) { 
		document.getElementById(id).src = '/img/nav_books_at.gif';
	} 
} 
function cleartab(id) { 
	if (dom) { 
		document.getElementById(id).src = '/img/nav_books.gif';
		document.getElementById('s1').src = '/img/nav_separator_at.gif';
		document.getElementById('s2').src = '/img/nav_separator_at.gif';
	} 
} 


function getStyleObject(objectId) {
	if(document.getElementById && document.getElementById(objectId)) {
		return document.getElementById(objectId).style;
	}
	else if (document.all && document.all(objectId)) {  
		return document.all(objectId).style;
	} 
	else if (document.layers && document.layers[objectId]) { 
		return document.layers[objectId];
	} else {
		return false;
	}
}

function changeObjectVisibility(objectId, newVisibility) {
    var styleObject = getStyleObject(objectId);
    if(styleObject) {
		styleObject.visibility = newVisibility;
		return true;
    } else {
		return false;
    }
}


function switchDiv(div_id) {
	var style_sheet = getStyleObject(div_id);
	if (style_sheet) {
	  	hideMailForm();
	  	changeObjectVisibility(div_id,"visible");
	} else {
	  	alert("Please try again with a current browser.");
	}
}

function hideMailForm() {
	changeObjectVisibility('mail_list_signup',"hidden");
}

function findPosition(object) {									
	var positionLeft = positionTop = 0;							
	if (object.offsetParent) {									
		do {
			positionLeft += object.offsetLeft;					
			positionTop += object.offsetTop;
		} while (object = object.offsetParent);
	}
	return [positionLeft,positionTop];							
}

function closeLayer(myLayer) {
	document.getElementById(myLayer).style.display='none';
}

function openML(object, myLayer, id)	{
	var coords = findPosition(object);							
	var layer = document.getElementById(myLayer);
	layer.style.top = 40+ coords[1] + 'px'; 					
	layer.style.left = -165 + coords[0] + 'px';					
	document.getElementById(myLayer).style.display='inline';	
	changeObjectVisibility(myLayer,"visible");
}

function openML1(object, myLayer, id)	{
	var coords = findPosition(object);							
	var layer = document.getElementById(myLayer);
	layer.style.top =-600+ coords[1] + 'px'; 					
	layer.style.left = -185 + coords[0] + 'px';
	document.getElementById(myLayer).style.display='inline';	
	changeObjectVisibility(myLayer,"visible");
}


sfHover = function() {
	var sfEls = document.getElementById("nav").getElementsByTagName("LI");
	for (var i=0; i<sfEls.length; i++) {
		sfEls[i].onmouseover=function() {
			this.className+=" sfhover";
		}
		sfEls[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", sfHover);