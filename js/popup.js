function openLayer(object, myLayer, id, x, y, direction) {
	var coords = findPosition(object);							
	var layer = document.getElementById(myLayer);
	if (direction == "l") {
		layer.style.top = coords[1] - x + 'px'; 					
		layer.style.left = y + coords[0] + 'px';
	} else {
		layer.style.top = coords[1] - x + 'px'; 					
		layer.style.left = y + coords[0] + 'px';
	}					
	document.getElementById(myLayer).style.display='inline';	
	
	var content = document.getElementById(id).innerHTML;
	document.getElementById('popwin').innerHTML = content;
}

function hideLayer() {					
	document.getElementById('popwin').style.display='none';	
}