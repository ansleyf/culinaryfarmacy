<?php
/*
Template Name: Food & Drink Menu
*/
?>

<?php get_header(); ?>

<div id="container">

	<div id="content_border">
	
		<?php if(has_post_thumbnail()) { ?>
		<div id="featurebanner"><?php the_post_thumbnail( 'page-feature' ); ?></div>
		<?php } else { ?>
		<?php } ?>
	
		<div id="content" class="wide">
	    
	        <div class="postarea">
	        
	        	<h3 class="menu_title"><?php echo cat_id_to_name(of_get_option('category_menu1')); ?></h3>
	        	
				<?php $wp_query = new WP_Query(array('post_type'=>'food-menu', 'cat'=>of_get_option('category_menu1'), 'posts_per_page'=>of_get_option('postnumber_menu'))); ?>
				<?php if($wp_query->have_posts()) : while($wp_query->have_posts()) : $wp_query->the_post(); ?>
				<?php global $more; $more = 0; ?>
	            
	            <div class="menu_page">
	            
	            	<div class="menu_thumb">
	                    <?php the_post_thumbnail( 'menu-thumbnail' ); ?>
	                </div>
	            
	            	<div class="menu_description">              
	                    <h4><?php the_title(); ?><span><?php echo get_post_meta($post->ID, "_price", true); ?></span></h4>
	                    <?php the_excerpt(); ?>
			    <a href="<?php the_permalink(); ?>">Read More</a>
	                </div>
	            
	            </div>
	            
	            <div style="clear:both;"></div>
								
				<?php endwhile; ?>
				<?php endif; ?>
			
			</div>
			
		</div>
	    
	</div>
			
</div>

<?php if(of_get_option('display_menu2') == '1') { ?>
<div class="container_menu">

	<div class="border_menu">
	
		<div class="content_wide">
	    
	        <div class="postarea">
	        
	        	<h3 class="menu_title"><?php echo cat_id_to_name(of_get_option('category_menu2')); ?></h3>
	        	
				<?php $wp_query = new WP_Query(array('post_type'=>'food-menu', 'cat'=>of_get_option('category_menu2'), 'posts_per_page'=>of_get_option('postnumber_menu'))); ?>
				<?php if($wp_query->have_posts()) : while($wp_query->have_posts()) : $wp_query->the_post(); ?>
				<?php global $more; $more = 0; ?>
	            
	            <div class="menu_page">
	            
	            	<div class="menu_thumb">
	                    <?php the_post_thumbnail( 'menu-thumbnail' ); ?>
	                </div>
	            
	            	<div class="menu_description">              
	                    <h4><?php the_title(); ?><span><?php echo get_post_meta($post->ID, "_price", true); ?></span></h4>
	                    <?php the_content(); ?>
	                </div>
	            
	            </div>
	            
	            <div style="clear:both;"></div>
								
				<?php endwhile; ?>
				<?php endif; ?>
			
			</div>
			
		</div>
	    
	</div>
			
</div>
<?php } else { ?>
<?php } ?>

<?php if(of_get_option('display_menu3') == '1') { ?>
<div class="container_menu">

	<div class="border_menu">
	
		<div class="content_wide">
	    
	        <div class="postarea">
	        
	        	<h3 class="menu_title"><?php echo cat_id_to_name(of_get_option('category_menu3')); ?></h3>
	        	
				<?php $wp_query = new WP_Query(array('post_type'=>'food-menu', 'cat'=>of_get_option('category_menu3'), 'posts_per_page'=>of_get_option('postnumber_menu'))); ?>
				<?php if($wp_query->have_posts()) : while($wp_query->have_posts()) : $wp_query->the_post(); ?>
				<?php global $more; $more = 0; ?>
	            
	            <div class="menu_page">
	            
	            	<div class="menu_thumb">
	                    <?php the_post_thumbnail( 'menu-thumbnail' ); ?>
	                </div>
	            
	            	<div class="menu_description">              
	                    <h4><?php the_title(); ?><span><?php echo get_post_meta($post->ID, "_price", true); ?></span></h4>
	                    <?php the_content(); ?>
	                </div>
	            
	            </div>
	            
	            <div style="clear:both;"></div>
								
				<?php endwhile; ?>
				<?php endif; ?>
			
			</div>
			
		</div>
	    
	</div>
			
</div>
<?php } else { ?>
<?php } ?>

<?php if(of_get_option('display_menu4') == '1') { ?>
<div class="container_menu">

	<div class="border_menu">
	
		<div class="content_wide">
	    
	        <div class="postarea">
	        
	        	<h3 class="menu_title"><?php echo cat_id_to_name(of_get_option('category_menu4')); ?></h3>
	        	
				<?php $wp_query = new WP_Query(array('post_type'=>'food-menu', 'cat'=>of_get_option('category_menu4'), 'posts_per_page'=>of_get_option('postnumber_menu'))); ?>
				<?php if($wp_query->have_posts()) : while($wp_query->have_posts()) : $wp_query->the_post(); ?>
				<?php global $more; $more = 0; ?>
	            
	            <div class="menu_page">
	            
	            	<div class="menu_thumb">
	                    <?php the_post_thumbnail( 'menu-thumbnail' ); ?>
	                </div>
	            
	            	<div class="menu_description">              
	                    <h4><?php the_title(); ?><span><?php echo get_post_meta($post->ID, "_price", true); ?></span></h4>
	                    <?php the_content(); ?>
	                </div>
	            
	            </div>
	            
	            <div style="clear:both;"></div>
								
				<?php endwhile; ?>
				<?php endif; ?>
			
			</div>
			
		</div>
	    
	</div>
			
</div>
<?php } else { ?>
<?php } ?>

<?php get_footer(); ?>