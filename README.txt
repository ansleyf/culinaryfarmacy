Organic Restaurant Theme v3.1.1
http://www.organicthemes.com

INSTALL: 
1. Upload the theme by navigating to Appearance > Themes > Install Themes within the WordPress admin. Select the theme zip file to upload.
2. Within your WordPress dashboard select Appearance > Themes.
3. Select the Organic Restaurant theme.
4. Configure the theme options within Appearance > Theme Options.

CHANGELOG v1.1:
- Style updates
- Blog pagination update
- Menu page template update
- Added custom post types for menu items
- Added recent comment widget styling
- Fixed IE navigation display bug

CHANGELOG v2.0:
- Style updates
- Created multiple home page layouts

CHANGELOG v2.0.1:
- Fixed a minor bug with the slider that appeared only in Firefox when the browser was zoomed out less that 100%

CHANGELOG v2.0.2:
- Fixed a minor bug that occurred with the sidebar not being displayed on the 404 Template

CHANGELOG v3.0:
- Extensive design and development changes
- Options Framework integration
- Featured video integration
- Replacement of jFlow with Anything Slider
- Added color change options
- Made considerably easier to customize
- Added numbered pagination
- Much more

CHANGELOG v3.0.1:
- Added theme localization

CHANGELOG v3.0.2:
- Complete theme localization

CHANGELOG v3.0.3:
- Minor style adjustment to menu gradient

CHANGELOG v3.0.4:
- Removed an overflow hidden style that seemed to hide videos in the slider within Firefox

CHANGELOG v3.1:
- Added table of contents to stylesheet
- Changed Gravity Forms styling integration

CHANGELOG v3.1.1:
- Updated depreciated code "showposts" to "posts_per_page"
