<?php
/*
Template Name: Archive
*/
?>

<?php get_header(); ?>

<div id="container">

	<div id="content_border">
	
		<div id="content" class="left">
	    
	        <div class="postarea">
	    
	    		<h1><?php the_title(); ?></h1>
					
				<div class="archive_column">
		
					<h5><?php _e("By Page:", 'organicthemes'); ?></h5>
					<ul><?php wp_list_pages('title_li='); ?></ul>
				
					<h5><?php _e("By Month:", 'organicthemes'); ?></h5>
					<ul><?php wp_get_archives('type=monthly'); ?></ul>
							
					<h5><?php _e("By Category:", 'organicthemes'); ?></h5>
					<ul><?php wp_list_categories('sort_column=name&title_li='); ?></ul>
		
				</div>
				
				<div class="archive_column">
					
					<h5><?php _e("By Post:", 'organicthemes'); ?></h5>
					<ul><?php wp_get_archives('type=postbypost&limit=100'); ?> </ul>
					
				</div>
				            
	        </div>
			
		</div>
				
		<?php include(TEMPLATEPATH."/sidebar_right.php");?>
	
	</div>

</div>

<?php get_footer(); ?>