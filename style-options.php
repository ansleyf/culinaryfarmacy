<style type="text/css" media="screen">
<?php  
	$link_color = of_get_option('link_color'); 
	$link_hover_color = of_get_option('link_hover_color');
	$highlight_color = of_get_option('highlight_color');
?>

#container a, #container a:link, #container a:visited, #containerhome a, #containerhome a:link, #containerhome a:visited, #footer a, #footer a:link, #footer a:visited, #sidebar_right a, #sidebar_right a:link, #sidebar_right a:visited {
<?php 
	if ($link_color) {
	    echo 'color: ' .$link_color. '!important;';
    }; 
?>
}

#container a:hover, #container h1 a:hover, #container h2 a:hover, #container h3 a:hover, #container h4 a:hover, #container h5 a:hover, #container h6 a:hover, #containerhome a:hover, #containerhome h1 a:hover, #containerhome h2 a:hover, #containerhome h3 a:hover, #containerhome h4 a:hover, #containerhome h5 a:hover, #containerhome h6 a:hover, #footer a:hover, #sidebar_right ul.menu li a:hover, #sidebar_right ul.menu li ul.sub-menu li a:hover, #sidebar_right ul.menu .current_page_item a, #sidebar_right ul.menu .current-menu-item a, #footerwidgets ul.menu li a:hover, #footerwidgets ul.menu li ul.sub-menu li a:hover, 
 #footerwidgets ul.menu .current_page_item a, #footerwidgets ul.menu .current-menu-item a  {
<?php 
	if ($link_hover_color) {
	    echo 'color: ' .$link_hover_color. '!important;';
    }; 
?>
}

.more-link:hover, div.anythingSlider .thumbNav a:hover, div.anythingSlider .thumbNav a.cur, div.anythingSlider .arrow a:hover, #searchsubmit, #searchsubmit:hover, #commentform #submit:hover, .reply a:hover, .reply a:visited, #subbutton, #subbutton:hover {
<?php 
	if ($highlight_color) {
	    echo 'background-color: ' .$highlight_color. ';';
    }; 
?>
}
</style>