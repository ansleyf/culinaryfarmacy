<?php get_header(); ?>

<div id="containerhome">
	
	<div id="homeslider">
	
	    <div id="homeslider_border">
	    
	    	<?php if(of_get_option('display_slider') == '1') { ?>
	        <ul id="slider1">
	            <?php $wp_query = new WP_Query(array('cat'=>of_get_option('category_slider'),'posts_per_page'=>of_get_option('postnumber_slider'))); ?>
	            <?php if($wp_query->have_posts()) : while($wp_query->have_posts()) : $wp_query->the_post(); ?>
	            <?php $meta_box = get_post_custom($post->ID); $video = $meta_box['custom_meta_video'][0]; ?>
	            <?php global $more; $more = 0; ?>
	            <li>
	                <?php if ( $video ) : ?>
	                    <div class="feature_video"><?php echo $video; ?></div>
	                <?php else: ?>
	                    <a class="feature_img" href="<?php the_permalink() ?>" rel="bookmark"><?php the_post_thumbnail( 'home-feature' ); ?></a>
	                <?php endif; ?>
	                <div class="slideinfo">
	                	<h1><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>
	                	<?php $content = get_the_content(); echo wp_trim_words( $content , '70' ); ?>
	                	<a style="width:278px;" class="more-link" href="<?php the_permalink() ?>" rel="bookmark"><?php _e("Read More", 'organicthemes'); ?></a>
	                </div>
	                <h4><?php the_title(); ?></h4>
	            </li>
	            <?php endwhile; ?>
	            <?php else : // do not delete ?>
	    		<?php endif; // do not delete ?>
	        </ul>
	        <?php } else { ?>
	        <?php } ?>
	        
	    </div>
	    
	</div>
	
	<?php if(of_get_option('display_teasers') == '1') { ?>        
	<div id="teasers">
	
		<?php $wp_query = new WP_Query(array('cat'=>of_get_option('select_categories_teaser'),'posts_per_page'=>of_get_option('number_display_teaser'))); ?>
		<?php $post_class = 'first'; ?>
		<?php if($wp_query->have_posts()) : while($wp_query->have_posts()) : $wp_query->the_post(); ?>
		<?php global $more; $more = 0; ?>   
	
	    <div class="teaser <?php echo $post_class; ?>">
	    
			<div class="teaser_border">
			
				<?php
				    if ('first' == $post_class){
				      $post_class = 'second';
				    }elseif ('second' == $post_class){
				      $post_class = 'third';
				    }else{
				      $post_class = 'first';
				    }
				?>
	            
	            <a class="teaser-image" href="<?php the_permalink() ?>" rel="bookmark"><?php the_post_thumbnail( 'teaser-thumbnail' ); ?> <h2><?php the_title(); ?></h2></a>
	            <div class="teaserinfo">
	               
	            	<?php the_excerpt(); ?>
	        	</div>
	            <a style="width:278px;" class="more-link" href="<?php the_permalink() ?>" rel="bookmark"><?php _e("Read More", 'organicthemes'); ?></a>
	        
	        </div>
	
	    </div>
	    
	    <?php endwhile; ?>
	    <?php else : // do not delete ?>
	    <?php endif; // do not delete ?>
	            
	</div>
	<?php } else { ?>
	<?php } ?>
	
	<?php if(of_get_option('display_feature') == '1') { ?>
	<div id="homepage">
	
	    <div id="homepage_border">
	    
	        <div class="homepage_feature">
	            
	            <?php $recent = new WP_Query('page_id='.of_get_option('select_page_feature')); while($recent->have_posts()) : $recent->the_post();?>
	                
	            <h1 class="title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>
	            <?php the_content(__("Read More", 'organicthemes')); ?>
	                
	            <?php endwhile; ?>
	
	        </div>
	        
	        <div id="homeside">
	            <?php include(TEMPLATEPATH."/sidebar_home.php");?>
	        </div>
	    
	    </div>
	    
	</div> 
	<?php } else { ?>
	<?php } ?>

</div>

<?php get_footer(); ?>